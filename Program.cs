﻿using System;
using System.Collections.Generic;
namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            Console.WriteLine("Ex 22 - Part A - Array");
            
            string[] movieNames = new string[5] {
                "Warcraft: The Beginning", 
                "Ghost in the Shell", 
                "Blade", 
                "Ninja Scroll", 
                "Berserk: The Golden Age Arc III - Descent"};

            for (int i = 0; i < movieNames.Length; i++)
            {
                Console.WriteLine(movieNames[i]);
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part B - Change 1st & 3rd");

            movieNames[0] = "Berserk: The Golden Age Arc I - The Egg of the King";
            movieNames[2] = "Akira";

            for (int i = 0; i < movieNames.Length; i++)
            {
                Console.WriteLine(movieNames[i]);
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part C - Sort Array");
            Array.Sort(movieNames);

            for (int i = 0; i < movieNames.Length; i++)
            {
                Console.WriteLine(movieNames[i]);
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part D - Length");

            Console.WriteLine($"Array Length = {movieNames.Length}");

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part E - String.Join");
            Console.WriteLine(string.Join(", ", movieNames));

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part F - List");

            // favourite places to eat
            List<string> fpte = new List<string> {
                "Turkish 2 Go", 
                "Caffeine Bar", 
                "Subway", 
                "Zeytin On The Strand", 
                "The Pizza Library"};

            for (int i = 0; i < fpte.Count; i++)
            {
                Console.WriteLine(fpte[i]);
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part G - Sort List");

            fpte.Sort();

            for (int i = 0; i < fpte.Count; i++)
            {
                Console.WriteLine(fpte[i]);
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 22 - Part H - Remove 3rd");

            fpte.Remove(fpte[2]);

            for (int i = 0; i < fpte.Count; i++)
            {
                Console.WriteLine(fpte[i]);
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
